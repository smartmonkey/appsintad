import { NgModule, Optional, SkipSelf,ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { UserStoreService } from '../shared/user-store.service';

import { LoginService } from '../module/seguridad/login/login.service';

import { AuthGuard } from '../guards/auth.guard';


@NgModule({
  imports: [
    HttpClientModule,
    RouterModule
  ],
  providers: [
    
  ]
})
//https://angular.io/guide/ngmodule-faq
export class CoreModule {

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: CoreModule,
      providers: [LoginService,AuthGuard,UserStoreService]
    };
  }
 //Prevent reimport of the CoreModule
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
