import { DataProvider } from "../models/base/dataProvider.model";
import { Usuario } from "../models/seguridad/usuario.model";

import { Router, ActivatedRoute } from '@angular/router';
import { HttpParams } from '@angular/common/http'
import { LoginService } from "../module/seguridad/login/login.service";
import { FormControl } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators'
import { UserStoreService } from "../shared/user-store.service";
import { Component } from "@angular/core";

export const TOKEN_NAME: string = 'jwt_token';
//https://www.npmjs.com/package/ng2-drag-drop

@Component({
  template: ''
})
export class BaseUtilComponent {

  public DEBOUNCE_TIME: number = 400;
  public timerSubscription: any;
  public never: 'never';
  public showAccionAsociar = false;
  public showAccionCancelar = true;
  public showAccionNuevo = true;
  public showAccionModificar = true;
  public showAccionCheck = false;
  public showAccionEliminar = true;
  public isLinear = false;

  public translate: TranslateService = null;
  public _loginService: LoginService = null;

  public _userStoreService: UserStoreService

  /** El validar pagina. */
  public validarPagina: boolean = true;
  public iconAccionItems = [
    { text: 'Eliminar', icon: 'delete', click: 'confirmarEliminar' }
  ];

  /** La accion nuevo. */
  //@Input("accionNuevo") 
  public accionNuevo: boolean = false;  
  public validarCampo : boolean = false;
  /** El flag show frm edit. */
  public mostrarPanelForm: boolean = false;
  public touch: boolean;
  public term = new FormControl();
  public listasize: number = 0;
  public mensaje: string = '';
  public search: string = '';
  public titulo: string = '';
  public descripcion: string = '';
  public dataProvider: DataProvider = new DataProvider();
  public cantidadPage: number = 0;
  public isProgrress: boolean = false;
  public params = new HttpParams();
  public usuarioSession = new Usuario();

  constructor(public router: Router, public route: ActivatedRoute) {

  }
  public debounceTimeProcesar(): Observable<any> {
    return this.term.valueChanges.pipe(debounceTime(500), distinctUntilChanged());
  }

  public ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }



  public setUserStoreService(userStoreService: UserStoreService) {
    this._userStoreService = userStoreService;
  }

  public setTraslate(_translate: TranslateService): void {
    this.translate = _translate;
  }


  public obtenerNombrePersona(persona: any) {
    return persona.nombre + ' ' + persona.apellidoPaterno + ' ' + persona.apellidoMaterno;
  }

  public obtenerNombreAlumno(alumno: any) {
    return alumno.nombres + ' ' + alumno.apellidoPaterno + ' ' + alumno.apellidoMaterno;
  }

  public obtenerDescripcionItem(item: any): any {
    if (item != null) {
      if (item.codigoExterno != null && item.codigoExterno != '') {
        item.descripcionView = item.codigoExterno + ' ' + item.nombre;
      } else if (item.nombre != null && item.nombre != '') {
        item.descripcionView = item.nombre;
      }
    }
    return item;
  }



  public setDataProvider(data: any) {
    this.dataProvider.offset = this.dataProvider.pageSize;
    this.dataProvider.totalResults = data.contador;
  }


  public navigate(router: string) {
    this.router.navigate(['/' + router]);
  }

  public getToken(): string {
    return window.localStorage[TOKEN_NAME];
  }


  public abrirModalConfirDialog(dialogRef, mensajeModalAlert: string, aceptar?: string, cancelar?: string, titulo?: string) {
    dialogRef.componentInstance.mensaje = mensajeModalAlert;
    dialogRef.componentInstance.aceptar = aceptar == null ? "ACEPTAR" : aceptar;
    dialogRef.componentInstance.cancelar = cancelar == null ? "CANCELAR" : cancelar;
    dialogRef.componentInstance.titulo = titulo == null ? "" : titulo;
    return dialogRef;
  }
  public abrirModal(dialogRef, showModal: boolean, showSelectMultiple: boolean, esIncludeComponent: boolean, data: any, id?: string) {
    dialogRef.componentInstance.showModal = showModal;
    dialogRef.componentInstance.showSelectMultiple = showSelectMultiple;
    dialogRef.componentInstance.esIncludeComponent = esIncludeComponent;
    dialogRef.componentInstance.data = data;
    dialogRef.componentInstance.id = id;
    return dialogRef;
  }
  public getUsuarioSession() {  
    this.usuarioSession.idUsuario = this.getDataKey('idUsuario');    
    if (this.usuarioSession.idUsuario == null) {
      this.usuarioSession.idUsuario = '';
    }    
    this.usuarioSession.userName = this.getDataKey('userName');
    this.usuarioSession.nombre = this.getDataKey('nombre');
    this.usuarioSession.apellidoPaterno = this.getDataKey('apellidoPaterno');
    this.usuarioSession.apellidoMaterno = this.getDataKey('apellidoMaterno');
    this.usuarioSession.email = this.getDataKey('email');    
  }
  public getDataKey(Key): any {
    return window.localStorage['jwt' + Key];
  }
  public setDataKey(Key, data): any {
    return window.localStorage['jwt' + Key] = data;
  }
  public mensajeConfirmacionEliminar(mensaje?: string): string {
    return mensaje == '' ? "¿Esta seguro que desea eliminar?" : mensaje;
  }

  public startProgres() {
    this.isProgrress = true;
  }
  public endProgres() {
    this.isProgrress = false;
  }
  public limpiaDataProviderPage(search: string, pageSize: number) {
    this.dataProvider = new DataProvider();
    this.dataProvider.currentPage = 1;
    this.dataProvider.pageSize = pageSize;
    this.dataProvider.search = search;
    this.dataProvider.offset = pageSize;
  }

  public limpiaDataProviderModal(search: string) {
    this.limpiaDataProviderPage(search, 5);
  }
  public calcularStartRow() {
    this.dataProvider.startRow = (this.dataProvider.currentPage - 1) * this.dataProvider.pageSize;
  }

  public noEncontroRegistoAlmanecado(dataProviderTemp) {
    this.listasize = dataProviderTemp.totalResults;
    this.cantidadPage = dataProviderTemp.totalResults / this.dataProvider.pageSize;
    if (this.listasize > 0 && this.cantidadPage == 0) {
      this.cantidadPage = 1;
    } else {
      let cantidadPageEntero = Number.parseInt(this.cantidadPage + '');
      let resulDecimal = this.cantidadPage - cantidadPageEntero;
      if (resulDecimal > 0) {
        this.cantidadPage = this.cantidadPage + 1;
        this.cantidadPage = Number.parseInt(this.cantidadPage + '');
      } else {
        this.cantidadPage = Number.parseInt(this.cantidadPage + '');
      }
    }
  }


  /**
     * Método encargado de cargar mensajes del archivo messages.
     * @param clase Clase para ubicar los mensajes
     * @param cadena para traer el mensaje del properties
     * @param parametros Paramtros del mensaje
     * @return mensaje encontrado para la cadena
     */
  public cargarMensaje(key: string, ...parametros: string[]): string {
    try {
      console.log("cargarMensaje.key == > " + this.translate.instant(key));
      if (this.translate.instant(key) != null) {
        return; //ControlRecursosWeb.cargarMensajeParametro(this.translate.instant(key), parametros);
      }
    } catch (e) {
      //log.info(e.getMessage());
    }
    return '!' + key + '!';
  }

  /**
   * Metodo para ver un mensaje en pantalla.
   *
   * @param llave para buscar el mensaje en el messages.properties del paquete en el que se encuentra
   * @return mensaje del archivo messages.properties
   * @throws Exception al cargar el bundle
   */
  public cargarMensajeLlave(key: string): string {
    console.log("cargarMensajeLlave.key == > " + this.translate.instant(key));
    if (this.translate.instant(key) != null) {
      return this.translate.instant(key);
    }
    return '!' + key + '!';
  }

  /**
   * Metodo para ver un mensaje en pantalla.
   *
   * @param llave para buscar el mensaje en el messages.properties del paquete en el que se encuentra
   * @param parametros el parametros
   * @return mensaje del archivo messages.properties
   * @throws Exception al cargar el bundle
   */
  public cargarMensajeLlaveParams(key: string, ...parametros: string[]): string {
    console.log("cargarMensajeLlaveParams.key == > " + this.translate.instant(key));
    if (this.translate.instant(key) != null) {
      return ;//ControlRecursosWeb.cargarMensajeParametro(this.translate.instant(key), parametros);
    }
    return '!' + key + '!';
  }


}

