import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class BaseService {
  public url: string = "";
  public modulo: string = "";
  constructor(public http: HttpClient,public router : Router) {

   }
   
  get(path: string, paramsParam?: HttpParams): Observable<any> {
      return this.http.get(path, {params: paramsParam });
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(path,JSON.stringify(body));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(path,JSON.stringify(body));
  }
  postMultiPart(path: string, body: any = {}, headers : HttpHeaders): Observable<any> {
    return this.http.post(path,body);
  }

  delete(path: string, body: Object = {}): Observable<any> {
    return this.http.delete(path);
  }

  listar(searchDataPrvovider,params?: HttpParams, moduloPer ? : string) : Observable<any> {
    if (!params) {
       params = new HttpParams();
    }
    params = params.set("search", searchDataPrvovider.search);
    params = params.set("offset", searchDataPrvovider.offset );
    params = params.set("startRow", searchDataPrvovider.startRow );
    params = params.set("currentPage", searchDataPrvovider.currentPage );
    params = params.set("sortFields", searchDataPrvovider.sortFields );
    params = params.set("sortDirections", searchDataPrvovider.sortDirections );
    if (moduloPer != null) {
      this.modulo = moduloPer;
    }
    return this.get(this.modulo + this.url + "/listar",params);
  }

  contar(searchDataPrvovider, params ? : HttpParams, moduloPer ? : string) : Observable<any> {
    if (!params) {
       params = new HttpParams();
    }
    params = params.set("search", searchDataPrvovider.search);
    params = params.set("offset", searchDataPrvovider.offset );
    params = params.set("startRow", searchDataPrvovider.startRow );
    params = params.set("currentPage", searchDataPrvovider.currentPage );
    params = params.set("sortFields", searchDataPrvovider.sortFields );
    params = params.set("sortDirections", searchDataPrvovider.sortDirections );
    if (moduloPer != null) {
      this.modulo = moduloPer;
    }      
    return this.get(this.modulo + this.url + "/contar",params);
  }
  paginador(searchDataPrvovider, params ? : HttpParams, moduloPer ? : string) : Observable<any> {
    if (!params) {
       params = new HttpParams();
    }
    params = params.set("search", searchDataPrvovider.search);
    params = params.set("offset", searchDataPrvovider.offset );
    params = params.set("startRow", searchDataPrvovider.startRow );
    params = params.set("currentPage", searchDataPrvovider.currentPage );
    params = params.set("sortFields", searchDataPrvovider.sortFields );
    params = params.set("sortDirections", searchDataPrvovider.sortDirections );
    if (moduloPer != null) {
      this.modulo = moduloPer;
    }      
    return this.get(this.modulo + this.url + "/",params);
  }
  
  inicializar( params: HttpParams) : Observable<any> {
    return this.get(this.url,params);
  }

  buscarID(id) : Observable<any> {
    return this.get(this.url + "/" + id);
  }

  crear(data,moduloPer ? : string) : Observable<any>{
    if (moduloPer != null) {
      this.modulo = moduloPer;
    } 
    return this.post(this.modulo + this.url, data);
  }

  modificar(data,moduloPer ? : string) : Observable<any>{
    if (moduloPer != null) {
      this.modulo = moduloPer;
    } 
    return this.put(this.modulo + this.url ,data );
  }

  eliminar(id,moduloPer ? : string) : Observable<any> {   
    return this.delete(this.getUrl(id,moduloPer));
  }

  private getUrl(id,moduloPer ? : string){
    if (moduloPer != null) {
      this.modulo = moduloPer;
    } 
    return  this.modulo + this.url + "/" + id;
  }

  
}
