import {Component, EventEmitter, Input, Output, Optional,Inject,ReflectiveInjector} from '@angular/core';


import { Router,ActivatedRoute } from '@angular/router';
import { HttpParams } from '@angular/common/http'
import {FormControl,FormGroup, ValidatorFn} from '@angular/forms';

//import { environment } from 'environments/environment';
import { BaseUtilComponent } from './base.util.component';
import { BaseDialogContent } from './base.dialog.content.component';

//https://www.npmjs.com/package/ng2-drag-drop
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent extends BaseUtilComponent {	
 
 @Input("id") 
 public id : string = '';

 @Input("idTupla") 
 public idTupla : string = '';
 
 @Input("data") 
 public data : any;

 @Input("procesar") 
 public procesar : boolean = false;

 @Input("showModal") 
 public showModal : boolean = false;

 @Input("esIncludeComponent") 
 public esIncludeComponent : boolean = false;

 @Input("showSelectMultiple") 
 public showSelectMultiple : boolean = false;
	
//  @Input("dialogRef")  
//  public dialogRef: MatDialogRef<DialogContent>;

 // Usamos el decorador Output
 @Output("change") 
 public change = new EventEmitter();

 @Input("showComponentPage") 
 public showComponentPage : boolean = false;

 @Input("titlePage") 
 public titlePage : string = "";

 @Input("isCrud") 
 public isCrud : boolean = false;

 @Input("modulo") 
 public modulo : string = null;

 @Input("frmGroup") 
 public frmGroup: FormGroup;

 @Input("controlName") 
 public controlName: string;

 @Input("viewSource") 
 public viewSource: string;

 public rutaAccesoMenu : string  = "";

 public viewPage: string = "";

 constructor(public router :  Router, public route:  ActivatedRoute) {
   super(router,route);
   this.getUsuarioSession(); 
 }

 public getError(controlName: string): string {
  let error = '';
  const control = this.frmGroup.get(controlName);
  if (control != null && control.touched && control.errors != null) {
    /*if (control.errors?.requiered) {
      error = "Es requerido";
    } else if (control.errors?.existBD) {
      error = "No existe en bd";
    } else {*/
      error = JSON.stringify(control.errors);
    //}
  }
  return error;
}

public invalid(controlName: string): boolean {
  return this.frmGroup.get(controlName)?.invalid;
}

public showAccion() {
    if (this.showModal) {
        if (!this.isCrud) {
            this.showAccionNuevo = false;
            this.showAccionModificar = false;
            this.showAccionEliminar = false;
        }
  			if (this.showSelectMultiple) {
  				this.showAccionAsociar = true;
  				this.showAccionCheck = true;
  			}
		}
    if (this.esIncludeComponent && !this.isCrud) {
       this.showAccionNuevo = false;
    }
 }

// Cuando se lance el evento click en la plantilla llamaremos a este método
public cancelarPage() {
     this.mostrarPanelForm = false;
    // this.showComponentPage = false;
}

 public regresarPage() {
   this.lanzar(null);
   this.showComponentPage = false;
 }
 public regresarPageEvent(event) {
   this.lanzar(null);
   this.showComponentPage = false;
 }
 public regresarViewPage(event) {
   let isRegresar = event.isRegresar;
   if (isRegresar) {
     this.lanzar(event);
     this.showComponentPage = false;
   }
 }
public lanzar(event){
    // Usamos el método emit
     this.change.emit({data: this.data, isRegresar: true});
  }
	
	

public limpiaDataProvider(search : string,pageSize? : number) {
    if (this.showModal) {
          if (pageSize) {
              this.limpiaDataProviderPage(search ,pageSize);
          } else {
              this.limpiaDataProviderModal(search);
          }
         
    } else {
         if (pageSize) {
              this.limpiaDataProviderPage(search ,pageSize);
         } else {
              this.limpiaDataProviderPage(search ,8);
         }
    }
    this.calcularStartRow();
  }

  async guardoExito() {

    // let dialogRef = this._dialog.open(DialogMsgContent);
    // dialogRef.componentInstance.mensaje = "se guardo con exito";
    // dialogRef.afterClosed().subscribe(result => {
    //   dialogRef = null;
    // });
    this.startProgres();
							setTimeout(() => {	
								this.endProgres();
							  }, 4000);
  }
  async actualizadoExito() {

    this.startProgres();
    setTimeout(() => {	
      this.endProgres();
      }, 4000);
    // let dialogRef = this._dialog.open(DialogMsgContent);
    // dialogRef.componentInstance.mensaje = "se modifico con exito";
    // dialogRef.afterClosed().subscribe(result => {
    //   dialogRef = null;
    // });
  }
  async eliminoExito() {  
    // let dialogRef = this._dialog.open(DialogMsgContent);
    // dialogRef.componentInstance.mensaje = "se elimino con exito";
    // dialogRef.afterClosed().subscribe(result => {
    //   dialogRef = null;
    // });
  }
 public isProcesoOK(e) : boolean {
     if (e.error) {
       if (e.codigoError = "MSG") {
        this.mostrarMensajeAdvertencia(e.mensajeError);
       } else {
          this.mostrarMensajeError(e);
       }       
       return false;
     }
     return true;
  }
  public mostrarMensajeErrorFrmInvalid() {
    
    this.mostrarMensajeError("Formulario No valido");
  }
  public mostrarMensajeError(e) {
	//console.error('Error:' + e );
   let mensaje = '' + e;
   let codigo = '';
   console.log("e.status == " + e.status);
   console.log("e.code == " + e.code);
   console.log("e.statusCode == " + e.statusCode);

    if ( e != null && e != '' && e.mensajeError) {
        mensaje = '' + e.mensajeError;
        codigo = '' + e.codigoError;
    }
    if ( e != null && e != '' && e.statusText) {
      mensaje = '' + e.statusText;
      codigo = '' + e.status;
    }
    
    if ( e!= null && e.status) {
      if ( e!= null && e.status === 401 || e.status === 403) {
        this.endProgres();
        this.mostrarMensajeAdvertencia("Expiro la sessión");
        this.navigate("login");
        return;
      }
      if ( e!= null && e.code === 401 || e.code === 403) {
        this.endProgres();
        this.mostrarMensajeAdvertencia("Expiro la sessión");
        this.navigate("login");
        return;
      }
      if ( e!= null && e.code === 500) {
        this.endProgres();
        mensaje = "Error Interno en el Servidor status = " + e.code;
        this.mostrarMensajeAdvertencia(mensaje);
       // this.navigate("login");
        return;
      }
      if ( e!= null && e.status === 500) {
        this.endProgres();
        mensaje = "Error Interno en el Servidor status = " + e.status;
        this.mostrarMensajeAdvertencia(mensaje);
       // this.navigate("login");
        return;
      } 
      if ( e!= null && e.statusCode === 401 || e.statusCode === 403) {
        console.log("e!= null && e.statusCode ");
        this.endProgres();
        this.mostrarMensajeAdvertencia("Expiro la sessión");
        this.navigate("login");
        return;
      }

      if ( e!= null && e.statusCode === 500 ) {
        console.log("e!= null && e.statusCode ");
        this.endProgres();
        mensaje = "Error Interno en el Servidor status = " + e.statusCode;
        this.mostrarMensajeAdvertencia(mensaje);
        this.navigate("login");
        return;
      }
      
    }
    // let dialogRef = this._dialog.open(DialogMsgContent);
    //  dialogRef.componentInstance.title = 'ERROR';
    // dialogRef.componentInstance.mensaje = mensaje;
    // dialogRef.componentInstance.codigo = codigo;
    // dialogRef.afterClosed().subscribe(result => {
    //  dialogRef = null;
    //  this.endProgres();
    // });
  }

  public mostrarMensajeExito(msg) {
	  // let dialogRef = this._dialog.open(DialogMsgContent);
    // dialogRef.componentInstance.mensaje = msg;
    // dialogRef.afterClosed().subscribe(result => {
    //  dialogRef = null;
    // });
  }
  
  async  mostrarMensajeAdvertencia(msg) {
	  // let dialogRef = this._dialog.open(DialogMsgContent);
    // dialogRef.componentInstance.mensaje = msg;
    // dialogRef.afterClosed().subscribe(result => {
    //  dialogRef = null;
    // });
  }   
  
}

export class ValidatorsPer {
	static  existBD(controlValidar : string) : ValidatorFn {

		return (control: FormGroup) => {
			if (control != null) {

				if (control.root != null && control.root.get(controlValidar) != null
					&& control.root.get(controlValidar).value == null) {
					
					return { noExitBD: true };
				}
				if (control.root != null && control.root.get(controlValidar) != null
					) {
						console.log("2control.root.get(controlValidar).value " + control.root.get(controlValidar).value);
				}
				return { };
			}
		};
	}
}

@Component({
  template: `
    <!-- <p *ngIf="title != ''" >{{title}}</p>
  <mat-dialog-content>
    <p *ngIf="codigo != '' " >codigo : {{codigo}}</p>
    <p>{{mensaje}}</p>
  </mat-dialog-content>
  <mat-dialog-actions >
    <button mat-button (click)="dialogRef.close('')" color="accent" >{{aceptar}}</button>
  </mat-dialog-actions> -->
  `,
})
export class DialogMsgContent {
  public title : string = '';
  public aceptar : string = 'Aceptar';
  public codigo : string = '' ;
  public mensaje : string = '';
 // constructor(@Optional() public dialogRef: MatDialogRef<DialogMsgContent>) { }
}

@Component({
  template: `
  <!-- <mat-dialog-content>
    <p>{{mensaje}}</p>
  </mat-dialog-content>
  <mat-dialog-actions>
   <button mat-button (click)="dialogRef.close(false)" color="accent">{{cancelar}}</button>
	 <button mat-button (click)="dialogRef.close(true)" color="accent" >{{aceptar}}</button>
   </mat-dialog-actions> -->
  `,
})
export class DialogConfirmContent {
 public title : string = '';
 public aceptar : string = 'ELIMINAR';
 public cancelar : string = 'CANCELAR';
 public mensaje : string = '';
  //constructor(@Optional() public dialogRef: MatDialogRef<DialogConfirmContent>) { }
}

