export class BaseDialogContent {
    public showModal : boolean = false;
    public showSelectMultiple : boolean = false;
    public esIncludeComponent : boolean = false;
    public id : string;
    public data : any;
    public isCrud : boolean = false;
    public modulo : string = null;
}