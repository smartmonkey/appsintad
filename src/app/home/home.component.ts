
import {Component, EventEmitter, OnInit,OnChanges,SimpleChanges,AfterViewInit} from '@angular/core';
import {BaseComponent,DialogConfirmContent} from "../base/base.component";
import {FormControl} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends BaseComponent implements  OnInit, OnChanges,AfterViewInit {
	
	public folder: string;

  constructor(public router : Router,public route:  ActivatedRoute) { 
		super(router,route);
		
	}
	
	ngAfterViewInit() {
    // viewChild is set after the view has been initialized
	}

    ngOnChanges(changes: SimpleChanges) { 
		/*if (this.data && this.data.id) {
			this.params = this.params.set('id', this.data.id + '');
			this.buscar();
		}*/
		this.showAccion();
    }
	
	ngOnInit() {
		this.onInit();
	}
	 
	onInit() {
    /*var id = this.route.params.subscribe(params => {
      var id = params['id'];

    });*/
  }
  
  
  
}

