import { NgModule , ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { TranslateModule} from "@ngx-translate/core";

import { LoginComponent } from '../module/seguridad/login/login.component';
import { PaginacionComponent } from 'app/paginacion/paginacion.component';



@NgModule({
  imports: [  
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  declarations: [
   
   //EmailValidator,
   //NumberValidator,
   PaginacionComponent,
   LoginComponent,
  
  ],
  exports: [  
    PaginacionComponent,
    TranslateModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  


  
    LoginComponent,
    
 
  ],
  providers: [

  ]
})
//https://angular.io/guide/security
export class SharedModule { 

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }

}

 