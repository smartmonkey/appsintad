import { Observable } from 'rxjs';
 export class ViewComponent { 
     searchChangeObserver;
     onSearchChange(searchValue: string) {
         if (!this.searchChangeObserver) { 
             Observable.create(observer => { this.searchChangeObserver = observer; }).debounceTime(300) 
             // wait 300ms after the last event before emitting last event .distinctUntilChanged() 
             // only emit if value is different from previous value .subscribe(console.log); 
            } 
             this.searchChangeObserver.next(searchValue); 
        } 
    }