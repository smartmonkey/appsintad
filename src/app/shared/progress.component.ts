import {Component,OnInit,Optional,Input,ViewChild} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
//import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ProgressInterceptor} from './progress.interceptor';

export type ProgressBarColor = 'primary' | 'accent' | 'warn';

type ProgressBarMode = 'determinate' | 'indeterminate' | 'buffer' | 'query';

@Component({
  selector: 'pgs-progress-bar',
  template: `
  <!-- <div [class.loader-hidden]="!show">
    <div class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"></div>
    <div class="cdk-global-overlay-wrapper" style="justify-content: center; align-items: center;">
      <mat-spinner  [color]="color"  [value]="progressPercentage$ | async" ></mat-spinner>
   </div>
</div> -->
  `,
})
export class ProgressComponent implements OnInit {
  color = 'warn';
  value = 80;  
  //@ViewChild(MatDialog) private dialog: MatDialog;
  show = false;

  progressPercentage$: Observable<number>;

  //public dialogRef: MatDialogRef<DialogProgresContent>;

   constructor(private interceptor: ProgressInterceptor) { }

  ngOnInit() {
    try {
      this.progressPercentage$ = this.interceptor.progress$.pipe(
        map(progress => {
          this.show = false;
          if (progress === null) {
            this.setMode('indeterminate');
            this.show = true;
      //      this.openModal();
            return 0;
          } else {
            this.setMode('determinate');
            this.show = false;
    //        this.closeModal();
            return progress;
          }

        })
      );
    } catch (error) {
      
    }
  }
/*
  private openModal() {
    if (this.dialog != null) {
        this.dialogRef = this.dialog.open(DialogProgresContent);
        if (this.dialogRef != null) {
              this.dialogRef.componentInstance.esModal = true;
              this.dialogRef.afterClosed().subscribe(result => {
              if (result) {
               
                }
              });
        }
      }
    }
   private closeModal() {
     if (this.dialogRef != null) {
         this.dialogRef.close("");
     }
   }
  */
  private setMode(mode: ProgressBarMode) {
    // if (this.progressBar != null) {
    //   this.progressBar.mode = mode;
    // }
    
  }
}
/*
@Component({
  template: `
  <mat-progress-bar ngIf="esModal"  mode="indeterminate"  [color]="color" ></mat-progress-bar>
   `,
})
export class DialogProgresContent  {

  color = 'warn';
  //TODO:ver codigo
  public esModal: boolean = false;
  
 
  constructor(@Optional() public dialogRef: MatDialogRef<DialogProgresContent>) { }
}*/