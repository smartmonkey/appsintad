import { NgModule,ModuleWithProviders } from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule, TranslateLoader, MissingTranslationHandler,MissingTranslationHandlerParams } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { environment } from '../../environments/environment';
export function createTranslateLoader(http: HttpClient) {
 // return new TranslateStaticLoader(http, './assets/i18n', '.json');
 console.log("");
  let url =   'aas/langsRestController/i18n/';
  //console.log("url==>" + url);
  return new TranslateHttpLoader(http, url);
}

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(key: MissingTranslationHandlerParams) {
    let resultado : string = '!' + key.key + '!';
    if (resultado.toLowerCase().indexOf(".requiered".toLowerCase()) !== -1) {
        resultado = "requerido";
    } else if (resultado.toLowerCase().indexOf("!search!".toLowerCase()) !== -1) {
        resultado = "search";
    } else if (resultado.toLowerCase().indexOf(".search".toLowerCase()) !== -1) {
        resultado = "search";
    } else if (resultado.toLowerCase().indexOf(".no.existe.bd".toLowerCase()) !== -1) {
        resultado = "No existe en la Base de datos";
    }
    return resultado
  }
}

@NgModule({
  imports: [
    HttpClientModule,
   TranslateModule.forRoot({
    //isolate: true,
    loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
    }
    })
  ],
  exports: [TranslateModule],
  providers: [
     { provide: MissingTranslationHandler, useClass: MyMissingTranslationHandler }
  ],
})
export class AppTranslateModule { 

  static forRoot(): ModuleWithProviders<AppTranslateModule> {
    return {
      ngModule: AppTranslateModule,
      providers: []
    };
  }

}
