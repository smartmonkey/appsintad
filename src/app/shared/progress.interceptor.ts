import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,HttpHeaders,HttpEventType} from '@angular/common/http';

import { Observable,Subject,ReplaySubject} from 'rxjs';
import { tap} from 'rxjs/operators'

import { UserData } from './data.class';
import { UserStoreService } from './user-store.service';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
export const TOKEN_NAME: string = 'jwt_token';

//https://blog.jonrshar.pe/2017/Jul/15/angular-http-client.html
@Injectable()
export class ProgressInterceptor implements HttpInterceptor {

  public puerto: string = environment.apiPort;//"4000";//8084
  private urlBase: string = ""+ environment.apiHttp +"" + environment.apiIp +":"+ this.puerto + "/"+  environment.apiUrlConexto +"/rest/";
  public apiProxyUrl = this.urlBase;
  private authorization = '';

  public progress$: Observable<number | null>;
  private progressSubject: Subject<number | null>;

  constructor() {
    this.progressSubject = new ReplaySubject<number | null>(1);
    this.progress$ = this.progressSubject.asObservable();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   //this.setHeaders(req);
   let currentUrl : string = req.url;
   let esMultiPart = false;
   /*if (currentUrl.includes('fileUpload')) {
       esMultiPart = true;
   } */
   const headersParam = this.setHeaders(esMultiPart);
    const reportingRequest = req.clone({headers:headersParam, reportProgress: true});
    const handle = next.handle(reportingRequest);
 /*if (error.status === 401 || error.status === 403) {
      router.navigate(['/login']);
    }*/
    return handle.pipe(
      tap((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          this.progressSubject.next(null);
          break;
        case HttpEventType.DownloadProgress:
        case HttpEventType.UploadProgress:
          if (event.total) {
            this.progressSubject.next(Math.round((event.loaded / event.total) * 100));
          } else {
            this.progressSubject.next(100);
          }
          break;
        case HttpEventType.Response:
          this.progressSubject.next(100);
          break;
      }
    }),
    catchError(
      (error: any, caught: Observable<HttpEvent<any>>) => {
        console.log("catchError!!!! ..error " + error.statusText);
        console.log("catchError!!!! ..error.status " + error.status);
          if (error.status === 401) {
              this.handleAuthError();
              // if you've caught / handled the error, you don't
              // want to rethrow it unless you also want
              // downstream consumers to have to handle it as
              // well.
            //  return of(error);
          }
          if (error.status === 500) {
            this.handleAuthError();
            // if you've caught / handled the error, you don't
            // want to rethrow it unless you also want
            // downstream consumers to have to handle it as
            // well.
           // return of(error);
        }
        this.progressSubject.next(100);
        console.log("catchError!!!! ..status " + error.status);
        console.log("catchError!!!! ..statusText " + error.statusText);
          throw error;
      }
  )
  );

  }

   /**
     * Handle API authentication errors.
     */
    private handleAuthError() {
      console.log("handleAuthError ..error");
      // clear stored credentials; they're invalid
    //  this.auth.credentials = null;
      // navigate back to the login page
      //this.router.navigate(['/login']);
  }
  private getBase(request : HttpRequest<any>) : string {
    console.log("getBase....");
    let currentUrl : string = request.url;
    console.log("getBase....currentUrl " + currentUrl);
    let urlBase: string = ""+ environment.apiHttp +"" + environment.apiIp +":"+ this.puerto + "/"+  environment.apiUrlConexto +"/rest/";
    console.log("getBase....isMicroServicio " + environment.isMicroServicio);
    if (environment.isMicroServicio == false) {
      let serviceName : string  = currentUrl.substring(0,currentUrl.indexOf("/") )
      console.log("getBase....serviceName " + serviceName);
      if (currentUrl.includes(serviceName +'/')) {
         // urlBase = environment.apiServicio["API_URL_" + serviceName.toUpperCase()];
         urlBase = currentUrl;
          console.log(serviceName.toUpperCase());
      }
    }
    console.log("getBase....urlBase " + urlBase);
    this.apiProxyUrl = urlBase;
    return urlBase
  }

  private getProxyUrl(request : HttpRequest<any>) : string {
    this.getBase(request);
    let currentUrl : string = request.url;
    if (!currentUrl.includes('/'+ environment.apiUrlConexto +'/')) {
      if (environment.isMicroServicio == false) {
          currentUrl = this.apiProxyUrl + currentUrl.substring(currentUrl.indexOf("/") + 1 );
      } else {
          currentUrl = this.apiProxyUrl + currentUrl;
      }
    } 
    return currentUrl;
  }

  private setHeaders(esMultiPart : boolean) : HttpHeaders {
    let contentType = esMultiPart == true ? 'multipart/form-data;boundary=----WebKitFormBoundaryl4oxlVSt9yblG8VC' : 'application/json'
    let contentAcept = esMultiPart == true ? 'multipart/form-data' : 'application/json; charset=UTF-8'
    let headers =  new HttpHeaders({
      'Content-Type':''+ contentType +'',
      'Accept': ''+ contentAcept +'',
      'service_key': ''+environment.service_key+'',
      'auth_token': ''+ this.getToken() +'',
      'Authorization': 'Bearer '+ this.getToken() +'' ,
      'version': ''+environment.version+''
      
    });
    return headers;
  }

  private getToken(): string {
    return window.localStorage[TOKEN_NAME];
  }

}