import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
export const TOKEN_NAME: string = 'jwt_token';
/**
 * Servicio para mantener el estado de la aplicacion
 * Puede guardar el estado en el localstorage
 * Se comunica con la app mediante Observabales
 * */
export abstract class DataStoreService {

  /** Data Change emitter */
  private dataSubject: BehaviorSubject<any>;


  constructor(public localStoreKey: string, public data: any) {
    this.getData();
    this.dataSubject = new BehaviorSubject(this.data);
    this.emitChange();
  }
  /** Un observable al que suscribirse para recibir cambios */
  public getDataObservable = (): Observable<any> => this.dataSubject.asObservable();

  public setData(data) {
    this.saveData(data);
    this.emitChange();
  }

  private saveData(data) {
    this.data = data;
    if (this.localStoreKey) {
      localStorage.setItem(this.localStoreKey, JSON.stringify(this.data));
    }
  }

  private emitChange() {
    /** Notifies el siguente estado de la aplicación */
    this.dataSubject.next(this.data);
  }
  /**
   * Obtiene el estado actual
   */
  public getData(): any {
    this.loadData();
    return this.data;
  }
  private loadData() {
    if (this.localStoreKey) {
      let localData = localStorage.getItem(this.localStoreKey);
      if (localData) {
        this.data = JSON.parse(localData);
      }
    }
  }


  getToken(): String {
    return window.localStorage[TOKEN_NAME];
  }
  getDataKey(Key): any {
    return window.localStorage['jwt' + Key];
  }

 saveToken(token: String) {
    window.localStorage[TOKEN_NAME] = token;
  }
  saveTokenKey(key:string ,data: any) {
    window.localStorage['jwt' + key] = data;
  }
  destroyTokens() {
		this.destroyToken();
		this.destroyTokenKey('idUsuario');
		this.destroyTokenKey('userName');
		this.destroyTokenKey('nombre');
		this.destroyTokenKey('apellidoPaterno');
		this.destroyTokenKey('apellidoMaterno');
		this.destroyTokenKey('email'); 

    this.destroyTokenKey('nomcourier'); 
    this.destroyTokenKey('numruc'); 
    this.destroyTokenKey('codplaca'); 
    this.destroyTokenKey('nomtransportista'); 
    this.destroyTokenKey('codubigeodpto'); 
    this.destroyTokenKey('codubigeoprov'); 
    this.destroyTokenKey('codubigeodist'); 
    this.destroyTokenKey('dirpeticion'); 

    //this.destroyTokenKey('idTipoPersona');
    this.destroyTokenKey('idTipoUsuario');
    this.destroyTokenKey('idEmpresa');
    this.destroyTokenKey('idEntidad');
    this.destroyTokenKey('codigoEntidad');
		this.destroyTokenKey('nombreEmpresa');
		this.destroyTokenKey('rucEmpresa');
    this.destroyTokenKey('usuario');
    this.destroyTokenKey(TOKEN_NAME);
    this.destroyTokenKey('isData');
    this.destroyTokenKey('isDataPrivilegio');    
    this.destroyTokenKey('idAlumno');
    this.destroyTokenKey('idPersonal');
    this.destroyTokenKey('esUsuarioContable');
    this.destroyTokenKey('codigoExterno');
    this.destroyTokenKey('cantidadItentoConf');
    this.destroyTokenKey('cantidadItento');
    this.destroyTokenKey('offline');
    this.destroyTokenKey('idProveedor');
    this.destroyTokenKey('numtelefono');    
	}
   
  destroyToken() {
    window.localStorage.removeItem(TOKEN_NAME);
  }
  destroyTokenKey(key:string) {
    window.localStorage.removeItem('jwt' + key);
  }
}