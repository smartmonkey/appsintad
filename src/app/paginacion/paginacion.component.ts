import {Component, EventEmitter, Input, Output , OnInit,OnChanges,SimpleChanges,AfterViewInit, ViewEncapsulation} from '@angular/core';

import { BaseComponent } from '../base/base.component';

import { Router, ActivatedRoute } from '@angular/router';
import { DataProvider } from '../models/base/dataProvider.model';

@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.scss'],
})
export class PaginacionComponent extends BaseComponent implements OnInit, OnChanges,AfterViewInit {  
  //
//https://scotch.io/tutorials/3-ways-to-pass-async-data-to-angular-2-child-components
//http://learnangular2.com/lifecycle/
 public listasize : number = 0;
 
@Input("dataProvider") public dataProvider : DataProvider = new DataProvider();
@Input("cantidadPage")  public cantidadPage : number = 0;
// Usamos el decorador Output
@Output("change") change = new EventEmitter();

 public lastPageDisabled : boolean = true;
 public nextPageDisabled : boolean = true;
 public beforePageDisabled : boolean = true;
 public firstPageDisabled : boolean = true;

 constructor( public router : Router,public route:  ActivatedRoute) {
  super(router,route);
  }
  
  ngAfterViewInit() {
    // viewChild is set after the view has been initialized
        this.actualizar();
  }

  ngOnInit() {
      
    }

    ngOnChanges(changes: SimpleChanges) {
    
        this.actualizar();
      
    }

 public actualizar() {
    if (this.dataProvider != null) {
        this.listasize = this.dataProvider.totalResults;
        this.lastPageDisabled = true;
        this.firstPageDisabled = true;
        this.beforePageDisabled = true;
        this.nextPageDisabled = true;
        if (!(this.listasize > 0 && this.cantidadPage == 0)) {
            if (this.cantidadPage > 1) {
                this.lastPageDisabled = false;
                this.nextPageDisabled = false;
          }
        } 
     }
  }
  // Cuando se lance el evento click en la plantilla llamaremos a este método
 public lanzar(event){
    // Usamos el método emit
     this.change.emit({dataProvider: this.dataProvider});
  }

  
  
  public nextPage(event) {
    if (this.dataProvider.currentPage != this.cantidadPage) {
        this.dataProvider.currentPage = this.dataProvider.currentPage + 1;
        this.dataProvider.startRow = (this.dataProvider.currentPage - 1) *  this.dataProvider.pageSize;
        this.beforePageDisabled = false;
        this.firstPageDisabled = false;
        if (this.dataProvider.currentPage == this.cantidadPage) {
            this.nextPageDisabled = true;
            this.lastPageDisabled = true;
            this.beforePageDisabled = false;
            this.firstPageDisabled = false;
        }
    } else {
        if (this.dataProvider.currentPage == this.cantidadPage) {
            this.nextPageDisabled = true;
            this.lastPageDisabled = true;
        }
        this.beforePageDisabled = true;
        this.firstPageDisabled = false;
    }
    this.lanzar(event);
  }
  public beforePage(event) {
    this.nextPageDisabled = false;
    this.lastPageDisabled = false;
    if (this.dataProvider.currentPage > 0) {
        this.dataProvider.currentPage = this.dataProvider.currentPage - 1;
        this.dataProvider.startRow = (this.dataProvider.currentPage - 1) *  this.dataProvider.pageSize;
        if (this.dataProvider.currentPage == 1) {
            this.beforePageDisabled = true;
            this.firstPageDisabled = true;
        }
    } else {
      if (this.dataProvider.currentPage == 1) {
          this.beforePageDisabled = true;
          this.firstPageDisabled = true;
      }     
    }
    this.lanzar(event);
  }
  public firstPage(event) {
     this.dataProvider.currentPage = 1;
     this.dataProvider.startRow = (this.dataProvider.currentPage - 1) *  this.dataProvider.pageSize;
     this.beforePageDisabled = true;
     this.firstPageDisabled = true;
     this.lastPageDisabled = false;
     this.nextPageDisabled = false;
     this.lanzar(event);
  }

  public lastPage(event) {
     this.dataProvider.currentPage = this.cantidadPage;
     this.dataProvider.startRow = (this.dataProvider.currentPage - 1) *  this.dataProvider.pageSize;
     this.lastPageDisabled = true;
     this.nextPageDisabled = true;
     this.firstPageDisabled = false;
     this.beforePageDisabled = false; 
     this.lanzar(event);    
  }  
}