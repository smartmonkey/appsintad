import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { map, catchError, filter } from 'rxjs/operators'

import { Observable, Subject, ReplaySubject } from 'rxjs';
import { BaseService } from "../../../base/base.service";

import { Usuario } from "../../../models/seguridad/usuario.model";
import { Title } from '@angular/platform-browser';

import { TranslateService } from '@ngx-translate/core';

export const TOKEN_NAME: string = 'jwt_token';




@Injectable()
export class LoginService extends BaseService {
  usuarioSession: Usuario = new Usuario();
 
  constructor(public http: HttpClient, public router: Router, public activatedRoute: ActivatedRoute,
    private titleService: Title, public translate: TranslateService) {
    super(http, router);
    console.log("instanciando serviceseguridad")
    this.url = "sintad/login";

    // this.setTitleActiveMenuRouter();
  }
  
  public setTitle(title: string) {
    if (this.translate.instant(title) != null) {
      title = this.translate.instant(title);
    }
    this.titleService.setTitle("Build ERP :: " + title);
  }

  login(user: any): Observable<any> {
    return this.post(this.url, user);
  }
  logout(user: any): Observable<any> {
    return this.put(this.url, user);
  }
  

  public getToken(): string {
    return window.localStorage[TOKEN_NAME];
  }
  public getDataKey(Key): any {
    return window.localStorage['jwt' + Key];
  }

  public setDataKey(Key, data): any {
    return window.localStorage['jwt' + Key] = data;
  }

  
  isTokenExpired(token?: string): boolean {
    // console.log("isTokenExpired.token?" + token);
    if (!token) token = this.getToken();
    if (!token) return true;


    //console.log("isTokenExpired.token.paso" + token);
    /* const date = this.getTokenExpirationDate(token);
     if(date === undefined) return false;
     return !(date.valueOf() > new Date().valueOf());
     */
  }
  

}
