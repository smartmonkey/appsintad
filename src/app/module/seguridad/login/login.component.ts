import { Component, OnInit, Optional,ViewChild } from '@angular/core';

import {BaseComponent,DialogConfirmContent} from "../../../base/base.component";

import { Router,ActivatedRoute } from '@angular/router';

import {User} from "./user.model";
import {LoginService} from "./login.service";
import { UserStoreService } from '../../../shared/user-store.service';


//import { AppComponent } from '../app.component';

//https://toddmotto.com/angular-2-forms-template-driven
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {
 public usuario : User = new User();
 public returnUrl: string;
 public isLoginOK : boolean = false;
 public idUsuario : number = 0;


 private readonly _secret: any;

	constructor(public router : Router,public route:  ActivatedRoute,
	public loginService: LoginService,public userStoreService : UserStoreService){
       super(router,route);
	 
    }

	ngOnInit() {
	  this.cerrarSession();
	  
	}
	  
	cerrarSession() {
		try {
		  	  let token = this.getToken();
			  let email = this.userStoreService.getDataKey('email');
			  this.usuario.token = token;
		      this.usuario.email = email;
			  // this.userStoreService.destroyTokens();
			  if (token != null && token != '') {
					this.userStoreService.destroyTokens();
					this.getUsuarioSession();
					this.inicializar();
		   } else {
				this.inicializar();
		   }		   
		} catch (e) {
		  this.mostrarMensajeError(e);
		}
	   
	  }

	/**
	 * Nuevo.
	 *
	 */
	private limpiar () {
		this.usuario = new User();
		this.usuario.email = "torres.edto@gmail.com";
		this.usuario.token = "123456";
     	this.endProgres();
	}
	
	/**
	 * Inicializar.
	 *
	 */
	private inicializar() {
	 this.limpiar();		
	}
	
 public iniciarSession(invalid : any) {
	//alert("formulario no valido : " + invalid);
	 if (invalid) {
		this.mostrarMensajeErrorFrmInvalid();
		 return;
	 }
	  this.userStoreService.destroyToken();
     this.startProgres();

	 this.loginService.login(this.usuario).subscribe(
		dataRest => {
			if (this.isProcesoOK(dataRest)) {
				 this.data = dataRest.objetoResultado;
				 this.endProgres(); 
				this.userStoreService.saveToken(dataRest.authToken);	
				this.userStoreService.saveTokenKey('idUsuario',this.data.idUsuario);
				this.userStoreService.saveTokenKey('email',this.data.email);
				this.userStoreService.saveTokenKey('nombre',this.data.nombre);
				this.endProgres();
				this.navigate('home');
			}				
		},
		error => {
			this.mostrarMensajeError(error.message);
			this.endProgres();
			this.userStoreService.destroyTokens();
			this.isLoginOK = false;
			//this.appComponent.getUsuarioSession();
	   },
	   ()=> {
		 //termino servicio httpp
	   }
	  );
}

	
	
}
