import { Component, Optional, EventEmitter, OnInit, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


import { LoginService } from "../../seguridad/login/login.service";

import { BaseComponent } from "../../../base/base.component";

import { TipoContribuyenteService } from './tipocontribuyente.service';
import { TipoContribuyente } from 'app/models/sintad/tipocontribuyente.model';


/**
 * La Class tipoContribuyenteComponent.
 * <ul>
 * <li>Copyright 2017 ndavilal -
 * ndavilal. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author ndavilal
 * @version 1.0, Sat Dec 23 17:16:37 COT 2017
 * @since SIAA-CORE 2.1
 */

@Component({
	selector: 'app-tipoContribuyente',
	templateUrl: './tipoContribuyente.component.html',
	styleUrls: ['./tipoContribuyente.component.scss'],
	providers: [TipoContribuyenteService]
})
export class TipoContribuyenteComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {
	public tipoContribuyente: TipoContribuyente = new TipoContribuyente();
	public listarTipoContribuyente: TipoContribuyente[] = [];
	constructor(private fb: FormBuilder, public router: Router, public route: ActivatedRoute,
		public tipoContribuyenteService: TipoContribuyenteService, public _translate: TranslateService) {
		super(router, route);
		super.setTraslate(_translate);
		this.debounceTimeProcesar().subscribe(term => { this.search = term; this.buscar() });
	}

	ngAfterViewInit() {
		// viewChild is set after the view has been initialized
	}

	ngOnChanges(changes: SimpleChanges) {

	}

	ngOnInit() {
		this.onInit();
		this.inicializar();
		this.crearFormulario(this.tipoContribuyente);
	}

	onInit() {
		/*var id = this.route.params.subscribe(params => {
		  var id = params['id'];
	
		});*/
	}

	private crearFormulario(obj: TipoContribuyente): void {
		this.frmGroup = this.fb.group({
			idTipoContribuyente: [obj.idTipoContribuyente],			
			nombre: [obj.nombre, [Validators.required]],			
			estado: [obj.estado,[Validators.required]],
		});
		this.onChange();
	}
	private onChange(): void {

	}


	/**
		 * guardar.
		 *
		 */
	public guardar() {
		if (this.frmGroup.invalid) {		
			this.mostrarMensajeErrorFrmInvalid();
			return;
		}
		try {
			if (this.accionNuevo) {
				this.tipoContribuyenteService.crear(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.guardoExito();							
							this.buscar();
							
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			} else {
				this.tipoContribuyenteService.modificar(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.actualizadoExito();
							this.buscar();
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			}
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Nuevo.
	 *
	 */
	public nuevo() {
		this.tipoContribuyente = new TipoContribuyente();
		this.frmGroup.patchValue(this.tipoContribuyente, { onlySelf: true, emitEvent: false });
		this.mostrarPanelForm = true;
		this.accionNuevo = true;
	}

	/**
	 * Inicializar.
	 *
	 */
	private inicializar() {
		super.getUsuarioSession();
		this.limpiar();
	}

	/**
	 * Limpiar.
	 *
	 */
	private limpiar() {
		try {
			this.listarTipoContribuyente = [];
			this.limpiaDataProvider(this.search);
			this.tipoContribuyente = new TipoContribuyente();
			if (this.frmGroup != null) {
				this.frmGroup.patchValue(this.tipoContribuyente, { onlySelf: true, emitEvent: false });
			}
			this.buscar();
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar.
	 *
	 */
	public buscar() {
		try {
			this.listarTipoContribuyente = [];
			this.mostrarPanelForm = false;
			this.limpiaDataProvider(this.search);			
			this.tipoContribuyenteService.contar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.setDataProvider(data);
						this.listarTipoContribuyente = data.listaResultado;
						this.mostrarPanelForm = false;
						this.noEncontroRegistoAlmanecado(this.dataProvider);
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}
	/**
	  * cancelar.
	  *
	*/
	public cancelar() {
		this.mostrarPanelForm = false;
	}


	/**
	 * eliminar.
	 *
	 */
	public eliminar(id: any) {
		try {
			this.tipoContribuyenteService.eliminar(id).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.eliminoExito();
						this.buscar();
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}



	/**
	 * confirmar eliminar.
	 *
	 */
	public confirmarEliminar(tipoDocTemp: TipoContribuyente) {
		this.tipoContribuyente = tipoDocTemp;
	}


	
	/**
 * buscar id
 *
 */
	public buscarID(tipoDocTemp: TipoContribuyente) {
		try {
			if ((this.showAccionModificar && !this.esIncludeComponent) || this.showComponentPage) {
				this.tipoContribuyente = Object.assign({}, tipoDocTemp);
				this.mostrarPanelForm = true;
				this.accionNuevo = false;
			}
			this.frmGroup.patchValue(this.tipoContribuyente, { onlySelf: true, emitEvent: false });
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar paginado.
	 */
	private buscarPaginado() {
		if (this.dataProvider.totalResults > 0) {
			this.calcularStartRow();			
			this.tipoContribuyenteService.listar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {					
						this.listarTipoContribuyente = data.listaResultado;
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		}
	}

	/**
	   * getBufferedData.
	   *
	 */
	public getBufferedData(event) {
		this.dataProvider = event.dataProvider;		
		this.buscarPaginado();
	}

}



