import { NgModule,ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule }  from '../../../shared/shared.module';
import { TipoContribuyenteComponent } from './tipocontribuyente.component';

const routes: Routes = [{ path: '', component: TipoContribuyenteComponent }];
export const routing: ModuleWithProviders<any> = RouterModule.forChild(routes);
@NgModule({
  imports: [
   SharedModule,  
   routing
  ],
  declarations: [
    TipoContribuyenteComponent,    
  ],
  exports: [
    TipoContribuyenteComponent
  ],
  entryComponents: [],
  providers: [
  ]
})
export class TipoContribuyenteModule { }