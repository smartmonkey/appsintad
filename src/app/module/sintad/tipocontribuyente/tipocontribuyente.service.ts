
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import {BaseService} from "../../../base/base.service";
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class TipoContribuyenteService extends BaseService {

  constructor(public http: HttpClient,public router : Router) { 
    super(http,router);
    this.url = "sintad/tipoContribuyente";
  }
  listarTipoContribuyenteSelect(): Observable<any> {
    return this.get(this.url + "/listarTipoContribuyenteSelect");
  }
}
