import { NgModule,ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule }  from '../../../shared/shared.module';
import { EntidadComponent } from './entidad.component';

const routes: Routes = [{ path: '', component: EntidadComponent }];
export const routing: ModuleWithProviders<any> = RouterModule.forChild(routes);
@NgModule({
  imports: [
   SharedModule,  
   routing
  ],
  declarations: [
    EntidadComponent,    
  ],
  exports: [
    EntidadComponent
  ],
  entryComponents: [],
  providers: [
  ]
})
export class EntidadoModule { }