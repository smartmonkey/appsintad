import { Component, Optional, EventEmitter, OnInit, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


import { LoginService } from "../../seguridad/login/login.service";

import { BaseComponent } from "../../../base/base.component";


import { EntidadService } from './entidad.service';
import { Entidad } from 'app/models/sintad/entidad.model';
import { TipoDocumentoService } from '../tipodocumento/tipodocumento.service';
import { TipoContribuyenteService } from '../tipocontribuyente/tipocontribuyente.service';
import { TipoDocumento } from 'app/models/sintad/tipodocumento.model';
import { TipoContribuyente } from 'app/models/sintad/tipocontribuyente.model';


/**
 * La Class entidadComponent.
 * <ul>
 * <li>Copyright 2017 ndavilal -
 * ndavilal. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author ndavilal
 * @version 1.0, Sat Dec 23 17:16:37 COT 2017
 * @since SIAA-CORE 2.1
 */

@Component({
	selector: 'app-entidad',
	templateUrl: './entidad.component.html',
	styleUrls: ['./entidad.component.scss'],
	providers: [EntidadService,TipoDocumentoService,TipoContribuyenteService]
})
export class EntidadComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {
	public entidad: Entidad = new Entidad();
	public listarEntidad: Entidad[] = [];
	public listarTipoDocumento: TipoDocumento[] = [];
	public listarTipoContribuyente: TipoContribuyente[] = [];
	constructor(private fb: FormBuilder, public router: Router, public route: ActivatedRoute,public tipoContribuyenteService: TipoContribuyenteService,
		public entidadService: EntidadService, public tipoDocumentoService: TipoDocumentoService, public _translate: TranslateService) {
		super(router, route);
		super.setTraslate(_translate);
		this.debounceTimeProcesar().subscribe(term => { this.search = term; this.buscar() });
	}

	ngAfterViewInit() {
		// viewChild is set after the view has been initialized
	}

	ngOnChanges(changes: SimpleChanges) {

	}

	ngOnInit() {
		this.onInit();
		this.inicializar();
		this.crearFormulario(this.entidad);
	}

	onInit() {
		/*var id = this.route.params.subscribe(params => {
		  var id = params['id'];
	
		});*/
	}

	private crearFormulario(obj: Entidad): void {
		this.frmGroup = this.fb.group({
			tipoDocumento: this.fb.group({
				idTipoDocumento: [obj.tipoDocumento.idTipoDocumento, [Validators.required] ],
				nombre: [obj.tipoDocumento.nombre ,{ updateOn: 'blur' }]
			 }),
			 tipoContribuyente: this.fb.group({
				idTipoContribuyente: [obj.tipoContribuyente.idTipoContribuyente],
				nombre: [obj.tipoContribuyente.nombre ,{ updateOn: 'blur' }]
			 }),
			idEntidad: [obj.idEntidad],
			nroDocumento: [obj.nroDocumento, [Validators.required]],
			razonSocial: [obj.razonSocial, [Validators.required]],
			nombreComercial: [obj.nombreComercial],
			direccion: [obj.direccion],
			telefono: [obj.telefono],
			estado: [obj.estado, [Validators.required]],
		});
		this.onChange();
	}
	private onChange(): void {

	}


	/**
		 * guardar.
		 *
		 */
	public guardar() {
		if (this.frmGroup.invalid) {
			this.mostrarMensajeErrorFrmInvalid();
			return;
		}
		try {
			if (this.accionNuevo) {
				this.entidadService.crear(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.guardoExito();							
							this.buscar();
							
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			} else {
				this.entidadService.modificar(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.actualizadoExito();
							this.buscar();
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			}
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Nuevo.
	 *
	 */
	public nuevo() {
		this.entidad = new Entidad();
		this.frmGroup.patchValue(this.entidad, { onlySelf: true, emitEvent: false });
		this.mostrarPanelForm = true;
		this.accionNuevo = true;
	}

	/**
	 * Inicializar.
	 *
	 */
	private inicializar() {
		super.getUsuarioSession();
		this.limpiar();
		this.listarTipoDocumentoSelect();
		this.listarTipoContribuyenteSelect();
	}

	/**
	 * Limpiar.
	 *
	 */
	private limpiar() {
		try {
			this.listarEntidad = [];
			this.limpiaDataProvider(this.search);
			this.entidad = new Entidad();
			if (this.frmGroup != null) {
				this.frmGroup.patchValue(this.entidad, { onlySelf: true, emitEvent: false });
			}
			this.buscar();
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar.
	 *
	 */
	public buscar() {
		try {
			this.listarEntidad = [];
			this.mostrarPanelForm = false;
			this.limpiaDataProvider(this.search);			
			this.entidadService.contar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.setDataProvider(data);						
						this.listarEntidad = data.listaResultado;
						this.mostrarPanelForm = false;
						this.noEncontroRegistoAlmanecado(this.dataProvider);
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}
	public listarTipoDocumentoSelect() {
		try {
			this.listarTipoDocumento = [];						
			this.tipoDocumentoService.listarTipoDocumentoSelect().subscribe(
				data => {
					if (this.isProcesoOK(data)) {										
						this.listarTipoDocumento = data.listaResultado;
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}
	public listarTipoContribuyenteSelect() {
		try {
			this.listarTipoContribuyente = [];						
			this.tipoContribuyenteService.listarTipoContribuyenteSelect().subscribe(
				data => {
					if (this.isProcesoOK(data)) {										
						this.listarTipoContribuyente = data.listaResultado;
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}
	/**
	  * cancelar.
	  *
	*/
	public cancelar() {
		this.mostrarPanelForm = false;
	}


	/**
	 * eliminar.
	 *
	 */
	public eliminar(id: any) {
		try {
			this.entidadService.eliminar(id).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.eliminoExito();
						this.buscar();
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}



	/**
	 * confirmar eliminar.
	 *
	 */
	public confirmarEliminar(tipoDocTemp: Entidad) {
		this.entidad = tipoDocTemp;
	}


	
	/**
 * buscar id
 *
 */
	public buscarID(tipoDocTemp: Entidad) {
		try {
			if ((this.showAccionModificar && !this.esIncludeComponent) || this.showComponentPage) {
				this.entidad = Object.assign({}, tipoDocTemp);
				this.mostrarPanelForm = true;
				this.accionNuevo = false;
			}
			this.frmGroup.patchValue(this.entidad, { onlySelf: true, emitEvent: false });
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar paginado.
	 */
	private buscarPaginado() {
		if (this.dataProvider.totalResults > 0) {
			this.calcularStartRow();			
			this.entidadService.listar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {					
						this.listarEntidad = data.listaResultado;
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		}
	}

	/**
	   * getBufferedData.
	   *
	 */
	public getBufferedData(event) {
		this.dataProvider = event.dataProvider;		
		this.buscarPaginado();
	}

}



