
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import {BaseService} from "../../../base/base.service";

@Injectable()
export class EntidadService extends BaseService {

  constructor(public http: HttpClient,public router : Router) { 
    super(http,router);
    this.url = "sintad/entidad";
  }
}
