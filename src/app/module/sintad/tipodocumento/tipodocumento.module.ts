import { NgModule,ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule }  from '../../../shared/shared.module';
import { TipoDocumentoComponent } from './tipodocumento.component';

const routes: Routes = [{ path: '', component: TipoDocumentoComponent }];
export const routing: ModuleWithProviders<any> = RouterModule.forChild(routes);
@NgModule({
  imports: [
   SharedModule,  
   routing
  ],
  declarations: [
    TipoDocumentoComponent,    
  ],
  exports: [
    TipoDocumentoComponent
  ],
  entryComponents: [],
  providers: [
  ]
})
export class TipoDocumentoModule { }