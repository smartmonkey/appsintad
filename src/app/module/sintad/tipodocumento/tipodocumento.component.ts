import { Component, Optional, EventEmitter, OnInit, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


import { LoginService } from "../../seguridad/login/login.service";

import { BaseComponent } from "../../../base/base.component";

import { UserStoreService } from 'app/shared/user-store.service';
import { TipoDocumento } from 'app/models/sintad/tipodocumento.model';
import { TipoDocumentoService } from './tipodocumento.service';


/**
 * La Class tipoDocumentoComponent.
 * <ul>
 * <li>Copyright 2017 ndavilal -
 * ndavilal. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author ndavilal
 * @version 1.0, Sat Dec 23 17:16:37 COT 2017
 * @since SIAA-CORE 2.1
 */

@Component({
	selector: 'app-tipodocumento',
	templateUrl: './tipodocumento.component.html',
	styleUrls: ['./tipodocumento.component.scss'],
	providers: [TipoDocumentoService]
})
export class TipoDocumentoComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {
	public tipoDocumento: TipoDocumento = new TipoDocumento();
	public listarTipoDocumento: TipoDocumento[] = [];
	constructor(private fb: FormBuilder, public router: Router, public route: ActivatedRoute,
		public tipoDocumentoService: TipoDocumentoService, public userStoreService: UserStoreService, public _translate: TranslateService) {
		super(router, route);
		super.setTraslate(_translate);
		this.debounceTimeProcesar().subscribe(term => { this.search = term; this.buscar() });
	}

	ngAfterViewInit() {
		// viewChild is set after the view has been initialized
	}

	ngOnChanges(changes: SimpleChanges) {

	}

	ngOnInit() {
		this.onInit();
		this.inicializar();
		this.crearFormulario(this.tipoDocumento);
	}

	onInit() {
		/*var id = this.route.params.subscribe(params => {
		  var id = params['id'];
	
		});*/
	}

	private crearFormulario(obj: TipoDocumento): void {
		this.frmGroup = this.fb.group({
			idTipoDocumento: [obj.idTipoDocumento],
			codigo: [obj.codigo],
			nombre: [obj.nombre, [Validators.required]],
			descripcion: [obj.descripcion],
			estado: [obj.estado],
		});
		this.onChange();
	}
	private onChange(): void {

	}


	/**
		 * guardar.
		 *
		 */
	public guardar() {
		if (this.frmGroup.invalid) {
			this.mostrarMensajeErrorFrmInvalid();
			return;
		}
		try {
			if (this.accionNuevo) {
				this.tipoDocumentoService.crear(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.guardoExito();							
							this.buscar();
							
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			} else {
				this.tipoDocumentoService.modificar(this.frmGroup.value).subscribe(
					data => {
						if (this.isProcesoOK(data)) {
							this.actualizadoExito();
							this.buscar();
						}
					},
					error => {
						this.mostrarMensajeError(error);
					}
				);
			}
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Nuevo.
	 *
	 */
	public nuevo() {
		this.tipoDocumento = new TipoDocumento();
		this.frmGroup.patchValue(this.tipoDocumento, { onlySelf: true, emitEvent: false });
		this.mostrarPanelForm = true;
		this.accionNuevo = true;
	}

	/**
	 * Inicializar.
	 *
	 */
	private inicializar() {
		super.getUsuarioSession();
		this.limpiar();
	}

	/**
	 * Limpiar.
	 *
	 */
	private limpiar() {
		try {
			this.listarTipoDocumento = [];
			this.limpiaDataProvider(this.search);
			this.tipoDocumento = new TipoDocumento();
			if (this.frmGroup != null) {
				this.frmGroup.patchValue(this.tipoDocumento, { onlySelf: true, emitEvent: false });
			}
			this.buscar();
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar.
	 *
	 */
	public buscar() {
		try {
			this.listarTipoDocumento = [];
			this.mostrarPanelForm = false;
			this.limpiaDataProvider(this.search);			
			this.tipoDocumentoService.contar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.setDataProvider(data);
						this.listarTipoDocumento = data.listaResultado;
						this.mostrarPanelForm = false;
						this.noEncontroRegistoAlmanecado(this.dataProvider);
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}
	/**
	  * cancelar.
	  *
	*/
	public cancelar() {
		this.mostrarPanelForm = false;
	}


	/**
	 * eliminar.
	 *
	 */
	public eliminar(id: any) {
		try {
			this.tipoDocumentoService.eliminar(id).subscribe(
				data => {
					if (this.isProcesoOK(data)) {
						this.eliminoExito();
						this.buscar();
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}



	/**
	 * confirmar eliminar.
	 *
	 */
	public confirmarEliminar(tipoDocTemp: TipoDocumento) {
		this.tipoDocumento = tipoDocTemp;
	}


	
	/**
 * buscar id
 *
 */
	public buscarID(tipoDocTemp: TipoDocumento) {
		try {
			if ((this.showAccionModificar && !this.esIncludeComponent) || this.showComponentPage) {
				this.tipoDocumento = Object.assign({}, tipoDocTemp);
				this.mostrarPanelForm = true;
				this.accionNuevo = false;
			}
			this.frmGroup.patchValue(this.tipoDocumento, { onlySelf: true, emitEvent: false });
		} catch (e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Buscar paginado.
	 */
	private buscarPaginado() {
		if (this.dataProvider.totalResults > 0) {
			this.calcularStartRow();			
			this.tipoDocumentoService.listar(this.dataProvider, this.params).subscribe(
				data => {
					if (this.isProcesoOK(data)) {					
						this.listarTipoDocumento = data.listaResultado;
					}
				},
				error => {
					this.mostrarMensajeError(error);
				}
			);
		}
	}

	/**
	   * getBufferedData.
	   *
	 */
	public getBufferedData(event) {
		this.dataProvider = event.dataProvider;		
		this.buscarPaginado();
	}

}



