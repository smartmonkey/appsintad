import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';


import { LoginComponent } from './module/seguridad/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './guards/auth.guard';



//import { NoAuthGuard } from './auth/no-auth-guard.service'


const routes: Routes = [
  { path: '', pathMatch: 'full', component: LoginComponent },

  { path: 'not-found', component: NotFoundComponent, data: { title: 'not.fount.pnl.title' } },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], data: { title: 'home.pnl.title' } },
  { path: 'login', component: LoginComponent, data: { title: '' } },

  { path: 'tipoDocumento', loadChildren: () => import('./module/sintad/tipodocumento/tipodocumento.module').then(m => m.TipoDocumentoModule), canActivate: [AuthGuard]},

  { path: 'tipoContribuyente', loadChildren: () => import('./module/sintad/tipocontribuyente/tipocontribuyente.module').then(m => m.TipoContribuyenteModule), canActivate: [AuthGuard]},

  { path: 'entidad', loadChildren: () => import('./module/sintad/entidad/entidad.module').then(m => m.EntidadoModule), canActivate: [AuthGuard]},


  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
