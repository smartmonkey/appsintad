import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from '../module/seguridad/login/login.service';
import { UserStoreService } from '../shared/user-store.service';
import { User } from 'app/module/seguridad/login/user.model';


@Injectable()
export class AuthGuard implements CanActivate {

  private usuario : User = new User();

  constructor(
    private router: Router, 
    private authService: LoginService,
    private userStoreService : UserStoreService) {}

    canActivate() {   
      if (!this.authService.isTokenExpired()) { 
        return true;
      }
      try { 
        this.cerrarSessionServer();
      } catch (error) {
      }
      this.userStoreService.destroyTokens();  
      this.router.navigate(['/login']);    
      return false;
    }
    
    async cerrarSessionServer(): Promise<boolean> {
      let token = this.userStoreService.getToken();
      let email = this.userStoreService.getDataKey('email');
      this.usuario.token = token.toString();
      this.usuario.email = email;
      await this.authService.logout(this.usuario).toPromise().then(
        data => {
          this.userStoreService.destroyTokens();
        },
      );
      return Promise.resolve(true);
    }
  
}