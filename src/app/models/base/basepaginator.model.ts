

export class BasePaginator {  
  offset: number = null;
  startRow:number = null;
  search:string = null;
  checked : boolean = false;
  varCantidad : number = 0;
  idUsuarioLogin : string = "";
  esEliminado : boolean = false;
  id : Object = null;
  idEmpresaSelect : number = 0;
  descripcionView : string = null;
  authToken : string = null;
  idEntidadSelect : string = null;



  constructor() {
  
  }
}