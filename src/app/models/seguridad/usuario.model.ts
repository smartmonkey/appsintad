import {BasePaginator} from '../base/basepaginator.model';


export class Usuario extends BasePaginator {
 
    /** El id usuario. */
    idUsuario : string = null;
   
    /** El nombre. */
    nombre : string = null;
   
    /** El apellido paterno. */
    apellidoPaterno : string = null;
   
    /** El apellido materno. */
    apellidoMaterno : string = null;
   
    /** El email. */
    email : string = null;
   
    /** El telefono. */
    telefono : string = null;
   
    /** El celular. */
    celular : string = null;
   
    /** El user name. */
    userName : string = null;
   
    /** El user password. */
    userPassword : string = null;
   

   
    /** El codigo externo. */
    codigoExterno : string = null;
   
    /** El estado. */
    estado : string = null;

    
    /** El user password. */
    userPasswordEncriptado : string = null;

    nomcourier : string = null;
    numruc : string = null;
    codplaca : string = null;
    nomtransportista : string = null;
    codubigeodpto : string = null;
    codubigeoprov : string = null;
    codubigeodist : string = null;
    dirpeticion : string = null;






    /**
     * Instancia un nuevo usuario.
     */
    constructor() {
	  super();
    }
}