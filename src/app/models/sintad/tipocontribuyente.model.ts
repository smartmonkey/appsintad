import {BasePaginator} from '../base/basepaginator.model';

 

export class TipoContribuyente extends BasePaginator {
 
    /** El idTipoContribuyente de TipoContribuyente. */
    idTipoContribuyente : number = null;
   
    /** El nombre. */
    nombre : string = null;

    /** El estado. */
    estado : number = null;

    constructor() {
	  super();
    }
}