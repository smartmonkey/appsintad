import {BasePaginator} from '../base/basepaginator.model';
import { TipoContribuyente } from './tipocontribuyente.model';
import { TipoDocumento } from './tipodocumento.model';

export class Entidad extends BasePaginator {
 
    /** El idEntidad de Entidad. */
    idEntidad : number = null;

    tipoDocumento : TipoDocumento = new TipoDocumento();
   
    /** El nroDocumento. */
    nroDocumento : string = null;
   
    /** El razonSocial. */
    razonSocial : string = null;

    /** El nombreComercial. */
    nombreComercial : string = null;

    tipoContribuyente : TipoContribuyente = new TipoContribuyente();

    /** El direccion. */
    direccion : string = null;

    /** El telefono. */
    telefono : string = null;
    
    /** El estado. */
    estado : number = null;

    constructor() {
	  super();
    }
}