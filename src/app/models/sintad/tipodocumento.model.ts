import {BasePaginator} from '../base/basepaginator.model';

 

export class TipoDocumento extends BasePaginator {
 
    /** El idTipoDocumento de TipoDocumento. */
    idTipoDocumento : number = null;
   
    /** El codigo. */
    codigo : string = null;
   
    /** El nombre. */
    nombre : string = null;

    /** El descripcion. */
    descripcion : string = null;

    /** El estado. */
    estado : number = null;

    constructor() {
	  super();
    }
}