import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { BaseComponent } from './base/base.component';
import { ProgressComponent } from './shared/progress.component';
import { AppTranslateModule } from './shared/app-translate.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import localePy from '@angular/common/locales/es-PY';
import { registerLocaleData, DatePipe } from '@angular/common'; 
import { ProgressInterceptor } from './shared/progress.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
const interceptor = new ProgressInterceptor();
registerLocaleData(localePy, 'es');
@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,HomeComponent,
    BaseComponent,
    ProgressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AppTranslateModule.forRoot(),
    CoreModule.forRoot(), 
    SharedModule.forRoot(),  
  ],
  providers: [   
    { provide: ProgressInterceptor, useValue: interceptor },
    { provide: HTTP_INTERCEPTORS, useValue: interceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
