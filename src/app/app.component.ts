import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './base/base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { interval } from 'rxjs/internal/observable/interval';
import { UserStoreService } from './shared/user-store.service';
import { User } from './module/seguridad/login/user.model';
import { LoginService } from './module/seguridad/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit{ 
  openSidebar: boolean = true;
  public usuario : User = new User();
  
  menuList = [
    {
      nombre : "Tipo Documento",
      url: "tipoDocumento"
    },

    {
      nombre : "Tipo Contribuyente",
      url: "tipoContribuyente"
    },

    {
      nombre : "Entidad",
      url: "entidad"
    },

  ];
  

  constructor( public router : Router,public activatedRoute:  ActivatedRoute, public userStoreService : UserStoreService,public loginService: LoginService,) {
    super(router,activatedRoute);
    interval(1000).subscribe(() => {   
      this.getUsuarioSession();
  });
   }

  ngOnInit() {

  }

  showSubmenu(itemEl: HTMLElement) {
    itemEl.classList.toggle("showMenu");
  }

  cerrarSession() {
    try {
      let token = this.getToken();
  		let email = this.userStoreService.getDataKey('email');
  		this.usuario.token = token;
      this.usuario.email = email;
     // this.userStoreService.destroyTokens();
     if (token != null && token != '') {
    		  this.loginService.logout(this.usuario).subscribe(
    			    data => {
    					this.userStoreService.destroyTokens();
              this.endProgres();   
              this.navigate('login');  
    				},
    				error => {
                this.mostrarMensajeError(error.message);
                this.userStoreService.destroyTokens();
                this.endProgres();
                this.navigate('login');
    			   },
    			   ()=> {
             //termino servicio httpp
             this.userStoreService.destroyTokens();
             this.getUsuarioSession();
              this.navigate('login');
    			   }
          );
       } else {
        this.navigate('login');
       }
       
    } catch (e) {
      this.mostrarMensajeError(e);
    }
   
  }
}
