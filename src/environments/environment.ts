// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  isMicroServicio:false,   
  apiHttp:'http://',
  apiIp:'localhost',
  apiPort:'8085',
  apiServletDescarga:'DescargarReporte',
  apiUrlConexto: 'apisia',
  service_key:'3b91cab8-926f-49b6-ba00-920bcf934c2a',
  version:'1.0',
  config: {
    "API_URL": "",
    "VAPID_PUBLIC_KEY": "BHe82datFpiOOT0k3D4pieGt1GU-xx8brPjBj0b22gvmwl-HLD1vBOP1AxlDKtwYUQiS9S-SDVGYe_TdZrYJLw8"
  },
  apiServicio:{      
    "API_URL_SINTAD":"http://localhost:8080/sintad/",
  },
  apiServicioDescarga:{   
    "API_URL_DESCARGA_COMPARTIR":"",  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
